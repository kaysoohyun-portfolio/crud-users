import { User } from './interfaces/user';

export interface AppState {
  readonly users_list: User[];
}