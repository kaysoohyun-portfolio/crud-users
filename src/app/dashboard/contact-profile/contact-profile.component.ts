import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { SetUsers } from 'src/app/store/users.actions';
import { UserStore } from 'src/app/store/users.reducer';
import { State } from '../../store/index';

@Component({
  selector: 'app-contact-profile',
  templateUrl: './contact-profile.component.html',
  styleUrls: ['./contact-profile.component.sass']
})
export class ContactProfileComponent implements OnInit {
  userData = {
    name: "",
    lastname: "",
    nickname: "",
    email: "",
    phonenumber: "",
    address: "",
    birthday: "",
    company: "",
    job: "",
    notes: "",
  }
  userDataForm = this.formBuilder.group(this.userData);
  userList: UserStore;
  indexUser: number = null;
  indexFilter: number = null;
  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<ContactProfileComponent>,
    @Inject(MAT_DIALOG_DATA) public userDataEntry: any,
    private store: Store<State>
  ) {
    this.store.select('users').subscribe((state) => {
    this.userList = {
      user: [...state.user],
      userFilter: [...state.userFilter]
    };
  })}

  ngOnInit(): void {
    this.indexUser = this.userList.user.findIndex((item) => item.id === this.userDataEntry.user);
    this.indexFilter = this.userList.userFilter.findIndex((item) => item.id === this.userDataEntry.user);
    for (const key in this.userList.user[this.indexUser]) {
      if (key in this.userData) {
        this.userData[key] = this.userList.user[this.indexUser][key];
      }
    }
    this.userDataForm = this.formBuilder.group(this.userData);
  }

  onSaveUser() {
    if (this.userDataForm.valid) {
      Object.keys(this.userData).forEach((key) => {
        this.userData[key] = this.userDataForm.get(key).value;
      });

      if (this.indexUser >= 0) {
        this.userList.user[this.indexUser] = {
          ...this.userData,
          favorite: this.userList.user[this.indexUser].favorite,
          frecuency: this.userList.user[this.indexUser].frecuency,
          id: this.userList.user[this.indexUser].id,
        }

        if (this.indexFilter >= 0) {
          this.userList.userFilter[this.indexFilter] = {
            ...this.userData,
            favorite: this.userList.userFilter[this.indexFilter].favorite,
            frecuency: this.userList.userFilter[this.indexFilter].frecuency,
            id: this.userList.userFilter[this.indexFilter].id,
          }
        }
      } else {
        const newUser = {
          ...this.userData,
          favorite: false,
          frecuency: Math.floor(Math.random() * Math.floor(100)),
          id: this.userList.user.length,
        }
        this.userList.user.push(newUser);
        if (this.userList.userFilter.length > 0) {
          this.userList.userFilter.push(newUser);
        }
      }
      this.store.dispatch(new SetUsers(this.userList));
      this.dialogRef.close();
    }
  }

}
