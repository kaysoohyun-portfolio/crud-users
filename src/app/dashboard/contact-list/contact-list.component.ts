import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { User } from 'src/app/interfaces/user';
import { Store } from '@ngrx/store';
import { State } from '../../store/index';
import { SetUsers } from '../../store/users.actions';
import { UserStore } from 'src/app/store/users.reducer';
import { ContactProfileComponent } from '../contact-profile/contact-profile.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.sass']
})

export class ContactListComponent implements OnInit {
  userList: UserStore = null;
  displayedColumns: string[] = ['select', 'name', 'email', 'phone', 'favorite', 'delete'];
  dataSource: MatTableDataSource<User>;
  selection = new SelectionModel<User>(true, []);

  constructor(
    private store: Store<State>,
    public dialog: MatDialog
  ) {
    this.store.select('users').subscribe((state) => {
      this.userList = {
        user: [...state.user],
        userFilter: [...state.userFilter]
      };
      this.dataSource = new MatTableDataSource(this.userList.userFilter);
    })
  }

  ngOnInit(): void { }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(index?: number, row?: User): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${index + 1}`;
  }

  getFavoriteIcon(isFavorite) {
    return isFavorite ? "star" : "star_border";
  }

  onFavorite(user: User) {
    const indexUser = this.userList.user.findIndex((item) => item.id === user.id);
    if (indexUser >= 0) {
      const nextFavoriteValue = !this.userList.user[indexUser].favorite;
      this.userList.user[indexUser] = {
        ...this.userList.user[indexUser], favorite: nextFavoriteValue
      }

      const indexFilter = this.userList.userFilter.findIndex((item) => item.id === user.id);
      if (indexFilter >= 0) {
        this.userList.userFilter[indexFilter] = {
          ...this.userList.userFilter[indexFilter], favorite: nextFavoriteValue
        }
      }
      this.store.dispatch(new SetUsers(this.userList));
    }
  }

  onDelete(user: User) {
    const indexUser = this.userList.user.findIndex((item) => item.id === user.id);
    if (indexUser >= 0) {
      this.userList.user.splice(indexUser, 1);

      const indexFilter = this.userList.userFilter.findIndex((item) => item.id === user.id);
      if (indexFilter >= 0) {
        this.userList.userFilter.splice(indexFilter, 1);
      }
      this.store.dispatch(new SetUsers(this.userList));
    }
  }

  onEditUser(user: User) {
    let dialogData = {
      width: '50rem',
      disableClose: false,
      data: {
        user: user.id,
      }
    }

    let dialogRef = this.dialog.open(ContactProfileComponent, dialogData);
  }
}
