import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { User } from 'src/app/interfaces/user';
import { SetUsers } from 'src/app/store/users.actions';
import { UserStore } from 'src/app/store/users.reducer';
import { State } from '../../store/index';

@Component({
  selector: 'app-contact-filter',
  templateUrl: './contact-filter.component.html',
  styleUrls: ['./contact-filter.component.sass']
})
export class ContactFilterComponent implements OnInit {
  filterList: String[] = ['All contacts', 'Frequently contacted', 'Starred contacts']
  userList: UserStore;

  constructor(private store: Store<State>) {
    this.store.select('users').subscribe((state) => {
      this.userList = {
        user: [...state.user],
        userFilter: [...state.userFilter]
      };
    })
  }

  ngOnInit(): void {
  }

  onFilter(index) {
    if (index === 1) {
      this.userList.userFilter = this.userList.user.filter((item: User) => item.frecuency > 50);
    } else if (index === 2) {
      this.userList.userFilter = this.userList.user.filter((item: User) => item.favorite);
    } else {
      this.userList.userFilter = this.userList.user;
    }
    this.store.dispatch(new SetUsers(this.userList));
  }
}
