import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { Store } from '@ngrx/store';
import { State } from '../../store/index';
import { SetUsers } from '../../store/users.actions';
import { UserStore } from 'src/app/store/users.reducer';

@Component({
  selector: 'app-contact-search',
  templateUrl: './contact-search.component.html',
  styleUrls: ['./contact-search.component.sass']
})
export class ContactSearchComponent implements OnInit {
  userList: UserStore;

  constructor(private store: Store<State>) {
    this.store.select('users').subscribe((state) => {
      this.userList = {
        user: [...state.user],
        userFilter: [...state.userFilter]
      };
    })
  }

  ngOnInit(): void {
  }

  onSearch(searchValue: string) {
    searchValue = searchValue.toLowerCase();
    this.userList.userFilter = this.userList.user.filter((item: User) =>
      item.name.toLowerCase().includes(searchValue)
      || item.lastname.toLowerCase().includes(searchValue)
      || item.nickname.toLowerCase().includes(searchValue)
    );
    this.store.dispatch(new SetUsers(this.userList));
  }
}
