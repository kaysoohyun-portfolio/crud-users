import { Component, OnInit } from '@angular/core';
import users_list from "../../assets/users_list.json";
import { Store } from '@ngrx/store';
import { State } from '../store/index';
import * as UserActions from './../store/users.actions';
import { UserStore } from '../store/users.reducer';
import { ContactProfileComponent } from './contact-profile/contact-profile.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {
  user: UserStore = {
    user: [...users_list],
    userFilter: [...users_list],
  };

  constructor(
    private store: Store<State>,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.store.dispatch(new UserActions.SetUsers(this.user));
  }

  onAddUser() {
    let dialogData = {
      width: '50rem',
      disableClose: false,
      data: {
        user: null,
      }
    }

    let dialogRef = this.dialog.open(ContactProfileComponent, dialogData);
  }
}
