import { Action } from '@ngrx/store';
import { UserStore } from './users.reducer';

export const SET_USERS = 'Set user list';

export class SetUsers implements Action {
  readonly type = SET_USERS;
  constructor(public payload: UserStore) { }
}

export type Actions = SetUsers;