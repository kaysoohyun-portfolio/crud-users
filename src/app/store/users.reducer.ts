import { User } from '../interfaces/user';
import * as UserActions from './users.actions';

export interface UserStore {
  user: User[];
  userFilter: User[];
}

export const initialState: UserStore = { user: [], userFilter: [] };

export function usersReducer(state = initialState, action: UserActions.Actions) {
  switch (action.type) {
    case UserActions.SET_USERS:
      return {...state, user: [...action.payload.user], userFilter: [...action.payload.userFilter]};
    default:
      return state;
  }
}