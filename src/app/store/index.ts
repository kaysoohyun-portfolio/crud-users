import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromUser from './users.reducer';

export interface State {
  users: fromUser.UserStore;
};

export const reducers: ActionReducerMap<State> = {
  users: fromUser.usersReducer,
};

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];